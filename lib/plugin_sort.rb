module PluginSort
	def sort_column
		Plugin.column_names.include?(params[:sort]) ? params[:sort] : "last_updated"
	end

	def sort_direction
		%w[asc desc].include?(params[:dir]) ? params[:dir] : "desc"
	end
end