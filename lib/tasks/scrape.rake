desc "Scrape the BukkitDev Website"

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'timeout'

namespace :scrape do
  # Load the config file
  require_relative '../../config/scrape.rb'

  # Run a full scrape on all pages
  task :full => :environment do
    puts "[Full] Starting full scrape at #{Time.new.inspect}"
    devBukkitUpdate(1, false)
    puts "[Full] Ending scrape at #{Time.new.inspect}"
  end

  task :update => :environment do
    puts "[Update] Starting update at #{Time.new.inspect}"
    devBukkitUpdate(1, true)
    puts "[Update] Ending update at #{Time.new.inspect}"
  end

  task :files => :environment do
    puts "[Files] Starting files update at #{Time.new.inspect}"
    # Scrape and update files and download counts
    Plugin.all.each do |plugin|
      pluginUpdate(plugin, true)
    end
    puts "[Files] Ending files update at #{Time.new.inspect}"
  end
end

def openUrl(url)
  doc = nil
  begin
    doc = Nokogiri::HTML(open(url))
  rescue OpenURI::HTTPError => e
    # We can't do anything on an HTTP error
    puts "Error opening #{url} (#{e.message})"
    return nil
  rescue Timeout::Error => e
    # This should be a timeout, we can keep retrying
    puts "Timeout opening #{url} -- Retrying"
    sleep CONFIG[:sleep] and retry
  rescue
    puts "Unknown error opening #{url}"
    return nil
  end
  sleep CONFIG[:sleep]
  return doc
end

def fileUpdate(fUrl)
  puts "[Scrape::fileUpdate] fUrl => #{fUrl}"
  
  doc = openUrl(fUrl)
  return nil if doc.nil?

  factBox = doc.at_css('.standard-date').parent().parent()

  file = PluginFile.find_or_create_by_md5_sum(factBox.css('dd')[4].text)
  
  file.name = doc.at_css('.main-body').at_css('h1').inner_text().strip()
  changelog = doc.at_css('.content-box').at_css('p')
  file.changelog = ''
  file.changelog = changelog.text if not changelog.nil?
  file.uploaded = Time.at(doc.at_css('.standard-date')['data-epoch'].to_i).to_datetime
  file.download_url = factBox.at_css('a')['href']
  file.filename = factBox.at_css('a').text
  file.size = factBox.css('dd')[3].text
  file.status = factBox.at_css('.file-status').text
  file.release_type = factBox.at_css('.file-type').text
  file.download_count = factBox.css('dd')[7].text.gsub(/[^\d]/, '').to_i
  
  factBox.at_css('.comma-separated-list').css('li').each do |build|
    fBuild = Build.find_or_create_by_name(build.text)
    file.builds.push(fBuild)
  end
  file.save
  return file
end

def pluginUpdate(plugin, scrapeFiles, scrapeData = false)
  categoryRegEx = /.*?category=(.*)/
  # Scrape plugin data if required
  if scrapeData then
    pUrl = CONFIG[:site] + plugin.slug
    puts "[Scrape::pluginUpdate::plugin] pUrl => #{pUrl}"
    
    doc = openUrl(pUrl)
    # If the plugin doesn't exist, remove from DB
    if doc.nil? then
      plugin.authors.destroy_all
      plugin.categories.destroy_all
      plugin.plugin_files.destroy_all
      plugin.destroy
      return nil
    end

    plugin.name = doc.css('h1')[1].text.strip
    plugin.summary = "#{doc.css('div.content-box')[0].text.strip![0, 500].tr("\n", ' ')}..."
    plugin.banner = ''
    icon = doc.at_css('.project-default-image')
    plugin.banner = icon.at_css('img')['data-full-src'] if icon
  
    # Loops through "content-box" to get sidebar data
    doc.css('.content-box').each do |box|
      boxName = box.at_css('h3')
      next if boxName.nil?
      if boxName.text == 'Facts' then
        plugin.last_updated = Time.at(box.css('.standard-date')[1]['data-epoch'].to_i).to_datetime
        plugin.stage = box.at_css('.project-stage').text
  
        box.css('span').each do |span|
          next if span['data-value'].nil?
          plugin.downloads = span['data-value'].to_i
          break
        end
      
        plugin.categories.destroy_all
        box.at_css('.category-list').css('a').each do |category|
          pCategory = Category.find_or_create_by_slug(categoryRegEx.match(category['href'])[1])
          pCategory.name = category.text
          pCategory.description = category['title']
          pCategory.save()
          plugin.categories.push(pCategory)
        end
      end
      if boxName.text == 'Authors' then
        plugin.authors.destroy_all
        box.css('a.user').each do |author|
          pAuthor = Author.find_or_create_by_name(author.text)
          plugin.authors.push(pAuthor)
        end
      end
    end
  end
 
  # Scrape Files
  pUrl = CONFIG[:site] + plugin.slug + "/files/"
  puts "[Scrape::pluginUpdate::files] pUrl => #{pUrl}"

  doc = openUrl(pUrl)
  if doc.nil?
    puts "[Scrape::pluginUpdate::files] Error opening #{pUrl}. Assuming Deleted - Removing"
    plugin.authors.destroy_all
    plugin.categories.destroy_all
    plugin.plugin_files.destroy_all
    plugin.destroy
    return nil
  end

  regEx = /\/files\/(.*)/
  # Check if there are no files, return if so
  if (doc.at_css('.listing-none-found') != nil)
    puts "[Scrape::pluginUpdate::files] No Files"
    return
  end
  
  # Figure out if we're paginated
  page = 1
  lastPage = 1
  pagination = doc.at_css('.listing-pagination')
  if (pagination.at_css('a') != nil)
    lastPage = Integer(pagination.at_css('.listing-pagination-pages-next').previous_sibling().text)
  end
  plugin.downloads = 0
  # Loop through the pages/files, fetch file info
  while(page <= lastPage)
    doc.css('td.col-file').each do |file|
      plugin.downloads += file.parent().at_css('.col-downloads').text.gsub(/[^\d]/, '').to_i
      next if not scrapeFiles
      # We're going to skip files we already have, as that's the slowest part of scraping
      # Also avoids duplicate build_plugin_file entries
      uploaded = Time.at(file.parent().at_css('.standard-date')['data-epoch'].to_i)
      next if not plugin.plugin_files.find_by_uploaded(uploaded).nil?

      fUrl = pUrl + regEx.match(file.at_css('a')['href'])[1]
      pFile = fileUpdate(fUrl)
      plugin.plugin_files.push(pFile) if not pFile.nil?
    end
    page += 1
    if (page <= lastPage)
      doc = openUrl("#{pUrl}?page=#{page}")
      break if doc.nil?
    end
  end
  plugin.save
end

def devBukkitUpdate(startPage, quick)
  # Counting variables
  updatedPlugins = 0
  newPlugins = 0
  totalPlugins = 0
  clearPages = 0
  slugRegEx = /\/server-mods\/(.*?)\//
  categoryRegEx = /.*?category=(.*)/
  
  page = startPage
  lastPage = nil
  
  lastUpdate = Plugin.find(:all, :select => 'last_updated', :order => 'last_updated DESC', :limit => 1)[0]
  lastUpdate = lastUpdate.last_updated if not lastUpdate.nil?
  puts "Last updated: #{lastUpdate}"
  
  while ((lastPage == nil || page <= lastPage) && clearPages < CONFIG[:extraPages])
  # If this is the initial scrape, we sort by name so as to hopefully not
  # miss any plugins added while scraping.
    if (quick)
      pageUrl = "#{CONFIG[:site]}?page=#{page}"
    else
      pageUrl = "#{CONFIG[:site]}?page=#{page}&sort=name"
    end
    puts "[Scrape::devBukkitUpdate] pageUrl => #{pageUrl}"
    doc = openUrl(pageUrl)
    if doc.nil?
      puts "Error opening #{pageUrl}"
      continue
    end

    # Get the maximum page count
    if (lastPage == nil)
      lastPage = Integer(doc.at_css('.listing-pagination-pages').at_css('.listing-pagination-pages-next').previous_sibling().text)
    end
    
    doc.css('.row-joined-to-next').each do |plugin|
      slug = slugRegEx.match(plugin.at_css('.col-project').at_css('a')['href'])[1]
      pEntry = Plugin.find_or_create_by_slug(slug)
      pEntry.last_updated = Time.at(plugin.at_css('.col-date').at_css('.standard-date')['data-epoch'].to_i).to_datetime

      needScrape = false
      if (pEntry.name.nil?)
        pEntry.downloads = 0
        puts "New Plugin: #{slug}"
        newPlugins += 1
        needScrape = true
      elsif (lastUpdate.nil? || pEntry.last_updated > lastUpdate)
        puts "Updated Plugin: #{slug}"
        updatedPlugins += 1
        needScrape = true
      end

      # Fetch Data
      totalPlugins += 1
      pEntry.name = plugin.at_css('.col-project').text
      pEntry.slug = slug
      pEntry.stage = plugin.at_css('.col-status').text
      pEntry.summary = plugin.next_sibling().at_css('.summary').inner_html
      pEntry.banner = ''
      icon = plugin.at_css('.col-icon').at_css('a')
      pEntry.banner = icon.at_css('img')['data-full-src'] if icon
      
      # Fetch authors
      pEntry.authors.destroy_all
      authorlist = plugin.at_css('.col-user')
      authorlist.css('a').each do |author|
        pAuthor = Author.find_or_create_by_name(author.text)
        pEntry.authors.push(pAuthor)
      end
      
      # Fetch categories
      pEntry.categories.destroy_all
      catlist = plugin.at_css('.col-category')
      catlist.css('a').each do |category|
        pCategory = Category.find_or_create_by_slug(categoryRegEx.match(category['href'])[1])
        pCategory.name = category.text
        pCategory.description = category['title']
        pCategory.save()
        pEntry.categories.push(pCategory)
      end

      pEntry.save()

      if (needScrape)
       clearPages = 0
       pluginUpdate(pEntry, true)
      end
    end
    clearPages += 1 if quick
    page += 1
  end
  puts "Scraped a total of #{totalPlugins} plugins. #{newPlugins} plugins were new. #{updatedPlugins} plugins were updated."
end
