desc "Setup category custom labels"

namespace :category do

  task :tags => :environment do
  	Category.all.each do |category|
  		puts "Enter tag for #{category.name}: "
  		tag = STDIN.gets.strip
  		category.custom_name = tag if not tag.empty?
  		category.save
  	end
  end
end