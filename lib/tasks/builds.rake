desc "Temp task to remove duplicate builds for files"

namespace :builds do
	task :fix => :environment do
		ActiveRecord::Base.connection.execute("delete from builds_plugin_files where rowid not in (select max(rowid) from builds_plugin_files group by build_id, plugin_file_id)")
	end
end