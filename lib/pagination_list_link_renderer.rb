class PaginationListLinkRenderer < WillPaginate::ActionView::LinkRenderer

  protected

    def page_number(page)
      lc = (page == 1 || page == total_pages) ? nil : "visible-desktop"
      unless page == current_page
        tag(:li, link(page, page, :rel => rel_value(page)), :class => lc)
      else
        tag(:li, link(page, page, :rel => rel_value(page)), :class => "active")
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        tag(:li, link(text, page))
      else
        tag(:li, "<span>#{text}</span>", :class => 'disabled')
      end
    end

    def html_container(html)
      tag(:ul, html)
    end

    def gap
      
    end

end