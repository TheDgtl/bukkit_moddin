CONFIG = {
  :site => 'http://dev.bukkit.org/server-mods/', # Site we are scraping
  :sleep => 1, # Amount to sleep between page loads
  :extraPages => 5 # Number of blank pages to scrape before stopping
}
