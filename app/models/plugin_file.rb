class PluginFile < ActiveRecord::Base
  attr_accessible :changelog, :download_count, :download_url, :filename, :md5_sum, :name, :size, :status, :type, :uploaded

  belongs_to :plugin
  has_and_belongs_to_many :builds

  default_scope :order => "uploaded DESC"
end
