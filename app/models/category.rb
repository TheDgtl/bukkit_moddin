class Category < ActiveRecord::Base
  attr_accessible :description, :name, :slug, :custom_name

  has_and_belongs_to_many :plugins

  default_scope :order => "name ASC"

  # Specify the slug as the link URL
  def to_param
  	slug
  end
end
