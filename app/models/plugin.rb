class Plugin < ActiveRecord::Base
  attr_accessible :banner, :downloads, :name, :slug, :stage, :summary

  has_and_belongs_to_many :categories
  has_and_belongs_to_many :authors
  has_many :plugin_files
  has_many :builds, :through => :plugin_files

  def to_param
  	slug
  end

  def self.search(page = 1, sort = nil, category = nil, author = nil, title = nil)
    condition = []
    args = {}
    cat = nil
    plugins = self

    # Start with a base of the categories plugins if defined
    if not category.nil? and not category.empty? and not category == '-1'
      cat = Category.find_by_slug(category)
      plugins = cat.plugins if not cat.nil?
    end

    # Find all plugins by the author, create an array of their IDs. This is to make sure all authors show up for plugins.
    if not author.nil? and not author.empty?
      plugin_ids = plugins.all(:joins => :authors, :conditions => ['lower(authors.name) LIKE ?', "%#{author.downcase}%"]).map(&:id)
      condition.push 'plugins.id in (:plugin_ids)'
      args[:plugin_ids] = plugin_ids
    end

    # Add the title to the arguments if required
    if not title.nil? and not title.empty?
      condition.push 'lower(plugins.name) LIKE :title'
      args[:title] = "%#{title.downcase}%"
    end

    #Join the conditions and find all the required plugins
    conditions = condition.join(" AND ")
    plugins = plugins.paginate(:page => page, :per_page => 20).all(:include => [:categories, :authors, {:plugin_files => :builds}], :conditions => [conditions, args], :order => sort)

    return plugins
  end
end
