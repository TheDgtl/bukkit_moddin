class Author < ActiveRecord::Base
  attr_accessible :name

  has_and_belongs_to_many :plugins

  default_scope :order => "name ASC"

  def to_param
  	name
  end
end
