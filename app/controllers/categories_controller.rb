class CategoriesController < ApplicationController
  include PluginSort
  def show
  	@plugins = Category.find_by_slug(params[:id], :include => :plugins).plugins.paginate(:page => params[:page], :per_page => 20).all(:include => [:plugin_files, :authors, :categories, :builds], :order => "#{sort_column} #{sort_direction}")

  	respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @plugins }
  	end
  end
end
