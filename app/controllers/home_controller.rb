class HomeController < ApplicationController
  def index
    @plugins = Plugin.page(params[:page]).per_page(20).order('last_updated DESC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @plugins }
    end
  end
end
