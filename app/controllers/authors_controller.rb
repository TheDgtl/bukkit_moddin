class AuthorsController < ApplicationController
  include PluginSort
  def show
  	@author = Author.find(:first, :conditions => ['lower(name) = ?', params[:id].downcase ], :include => :plugins)
  	if not @author.nil?
    	@plugins = @author.plugins.paginate(:page => params[:page], :per_page => 20).all(:include => [:plugin_files, :authors, :categories, :builds], :order => "#{sort_column} #{sort_direction}") || []
    else
    	@plugins = []
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @plugins }
    end
  end
  def index

  end
end
