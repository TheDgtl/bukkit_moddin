class PluginsController < ApplicationController
  include PluginSort
  def index
  	# Search params
  	page = params[:page]
  	sort = "#{sort_column} #{sort_direction}"
  	category = params[:category]
  	author = params[:author]
  	title = params[:title]
  	if not (category.nil? && author.nil? && title.nil?)
    	@plugins = Plugin.search(page, sort, category, author, title)
    else
    	@plugins = Plugin.paginate(:page => params[:page], :per_page => 20).all(:include => [:categories, :authors, {:plugin_files => :builds}], :order => sort)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @plugins }
    end
  end
end
