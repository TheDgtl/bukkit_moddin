class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :init

  def init
  	@start_time = Time.now.usec
  	@categories = Category.all
  	@current_action = action_name
  	@current_controller = controller_name
  end
end
