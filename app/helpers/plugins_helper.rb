module PluginsHelper
  include PluginSort
  def getBuilds(plugin)
    return [] if plugin.plugin_files.first.nil?
    return plugin.plugin_files.first.builds
  end

  def getLatestBuild(plugin)
    return '' if plugin.plugin_files.first.nil?
    return '' if plugin.plugin_files.first.builds.first.nil?
    return "(#{plugin.plugin_files.first.builds.first.name})"
  end

  def getLatestDownload(plugin)
    return '' if plugin.plugin_files.first.nil?
    return link_to plugin.plugin_files.first.name, plugin.plugin_files.first.download_url
  end

  def sortable(column, title = nil, default_sort = "asc")
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil
    if column != sort_column then
      direction = default_sort
    else
      direction = (sort_direction == "asc") ? "desc" : "asc"
    end
	options = {:sort => column, :dir => direction}
    link_to title, params.merge(options), {:class => css_class}
  end
end
