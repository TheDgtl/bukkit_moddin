# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120618040300) do

  create_table "authors", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "authors", ["name"], :name => "index_authors_on_name", :unique => true

  create_table "authors_plugins", :id => false, :force => true do |t|
    t.integer "plugin_id"
    t.integer "author_id"
  end

  add_index "authors_plugins", ["plugin_id", "author_id"], :name => "index_authors_plugins_on_plugin_id_and_author_id"

  create_table "builds", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "builds", ["name"], :name => "index_builds_on_name", :unique => true

  create_table "builds_plugin_files", :id => false, :force => true do |t|
    t.integer "plugin_file_id"
    t.integer "build_id"
  end

  add_index "builds_plugin_files", ["plugin_file_id", "build_id"], :name => "index_builds_plugin_files_on_plugin_file_id_and_build_id"

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "custom_name"
  end

  add_index "categories", ["slug"], :name => "index_categories_on_slug", :unique => true

  create_table "categories_plugins", :id => false, :force => true do |t|
    t.integer "plugin_id"
    t.integer "category_id"
  end

  add_index "categories_plugins", ["plugin_id", "category_id"], :name => "index_categories_plugins_on_plugin_id_and_category_id"

  create_table "plugin_files", :force => true do |t|
    t.string   "name"
    t.text     "changelog"
    t.datetime "uploaded"
    t.string   "download_url"
    t.string   "filename"
    t.integer  "size"
    t.string   "status"
    t.string   "release_type"
    t.string   "md5_sum"
    t.integer  "download_count"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "plugin_id"
  end

  add_index "plugin_files", ["md5_sum"], :name => "index_plugin_files_on_md5_sum"
  add_index "plugin_files", ["uploaded"], :name => "index_plugin_files_on_uploaded"

  create_table "plugins", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "stage"
    t.text     "summary"
    t.integer  "downloads"
    t.string   "banner"
    t.integer  "categories_id"
    t.integer  "authors_id"
    t.integer  "plugin_files_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.datetime "last_updated"
  end

  add_index "plugins", ["authors_id"], :name => "index_plugins_on_authors_id"
  add_index "plugins", ["categories_id"], :name => "index_plugins_on_categories_id"
  add_index "plugins", ["plugin_files_id"], :name => "index_plugins_on_plugin_files_id"
  add_index "plugins", ["slug"], :name => "index_plugins_on_slug", :unique => true

end
