class CreatePlugins < ActiveRecord::Migration
  def change
    create_table :plugins do |t|
      t.string :name
      t.string :slug
      t.string :stage
      t.string :summary
      t.integer :downloads
      t.string :banner
      t.references :categories
      t.references :authors
      t.references :plugin_files

      t.timestamps
    end
    add_index :plugins, :categories_id
    add_index :plugins, :authors_id
    add_index :plugins, :plugin_files_id
  end
end
