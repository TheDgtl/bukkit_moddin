class ChangeSummaryColumn < ActiveRecord::Migration
  def up
    change_column :plugins, :summary, :text
  end

  def down
    change_column :plugins, :summary, :string
  end
end
