class AddBuildsPluginsJoinTable < ActiveRecord::Migration
  def up
  	create_table :builds_plugins, :id => false do |t|
  	  t.references :plugin, :build
  	end
  	add_index :builds_plugins, [:plugin_id, :build_id]
  end

  def down
  	drop_table :builds_plugins
  end
end
