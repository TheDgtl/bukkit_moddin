class AddAuthorsPluginsJoinTable < ActiveRecord::Migration
  def up
  	create_table :authors_plugins, :id => false do |t|
  	  t.references :plugin, :author
  	end
  	add_index :authors_plugins, [:plugin_id, :author_id]
  end

  def down
  	drop_table :authors_plugins
  end
end
