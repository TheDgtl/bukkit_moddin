class RenamePluginFilesType < ActiveRecord::Migration
  def up
  	rename_column :plugin_files, :type, :release_type
  end

  def down
  end
end
