class AddCategoriesPluginsJoinTable < ActiveRecord::Migration
  def up
  	create_table :categories_plugins, :id => false do |t|
  	  t.references :plugin, :category
  	end
  	add_index :categories_plugins, [:plugin_id, :category_id]
  end

  def down
  	drop_table :categories_plugins
  end
end
