class AddIndexes < ActiveRecord::Migration
	def change
		change_table :authors do |a|
			a.index :name, :unique => true
		end
		change_table :builds do |b|
			b.index :name, :unique => true
		end
		change_table :categories do |c|
	  	c.index :slug, :unique => true
	  end
	  change_table :plugin_files do |pf|
	  	pf.index :md5_sum
	  	pf.index :uploaded
	  end
	  change_table :plugins do |p|
  		p.index :slug, :unique => true
  	end
  end
end
