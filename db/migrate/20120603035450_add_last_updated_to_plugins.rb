class AddLastUpdatedToPlugins < ActiveRecord::Migration
  def change
  	add_column :plugins, :last_updated, :datetime
  end
end
