class CreatePluginFiles < ActiveRecord::Migration
  def change
    create_table :plugin_files do |t|
      t.string :name
      t.text :changelog
      t.datetime :uploaded
      t.string :download_url
      t.string :filename
      t.integer :size
      t.string :status
      t.string :type
      t.string :md5_sum
      t.integer :download_count

      t.timestamps
    end
  end
end
