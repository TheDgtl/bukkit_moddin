class AddBuildsFilesJoinTable < ActiveRecord::Migration
  def up
  	create_table :builds_plugin_files, :id => false do |t|
  	  t.references :plugin_file, :build
  	end
  	add_index :builds_plugin_files, [:plugin_file_id, :build_id]
  end

  def down
  	drop_table :builds_plugin_files
  end
end
